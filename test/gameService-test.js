var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var sinon = require('sinon')
var should = chai.should();
 
var wordService = require('../server/wordService') 
var g = require('../server/gameService');
var gameStates = require('../public/shared/gameStates')
var bot = require('../server/ghostBot')

chai.use(chaiAsPromised);

describe('gameService', function() {
  
    const WORD = 'hello';
    const PREFIX = 'hel';
    const NEXT_PREFIX = 'hell';
  
    beforeEach(function() {
        sinon.stub(wordService, 'existLongerWords', (r) => {
          if(r === PREFIX || r === NEXT_PREFIX) return Promise.resolve(true);
          return Promise.resolve(false);
        });
        sinon.stub(wordService, 'isValidPrefix', (r) => {
            if(r === PREFIX || r === NEXT_PREFIX || r === WORD)
                return Promise.resolve(true);

            return Promise.resolve(false);
        });
        sinon.stub(bot, 'getNextPrefix', (r) => {
            if(r === PREFIX) return Promise.resolve(NEXT_PREFIX);
            if(r === NEXT_PREFIX) return Promise.resolve(WORD);
        });
    });

    describe('playAgainstTheBot(word)', function () {
        it('should return RUNNING when the game is running', function () {
          var result = {
              msg : gameStates.RUNNING,
              result : null,
              word : 'hell'
          }        
          return g.playAgainstTheBot(PREFIX).should.become(result);
        });

        it('should return BOT_WINS when the player completes a word', function () {
          var result = {
              msg : gameStates.WORD_COMPLETED,
              result : gameStates.BOT_WINS,
              word : WORD
          }                    
          return g.playAgainstTheBot(WORD).should.become(result);
        });

        it('should return INVALID_WORD when the game completes due to a non-existent word', function () {
          var result = {
              msg : gameStates.INVALID_WORD,
              result : gameStates.BOT_WINS,
              word : "NotExists"
          }             
          return g.playAgainstTheBot("NotExists").should.become(result);
        });
    });
  
    afterEach(function() {
        wordService.existLongerWords.restore();
        wordService.isValidPrefix.restore();
        bot.getNextPrefix.restore();
    });
});