var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var sinon = require('sinon')
var should = chai.should();

var wordService = require('../server/wordService')
var ghostBot = require('../server/ghostBot')

const dictionary = {    
    b: ['bagels'],
    h: ['hello', 'hurrah', 'hurrahed', 'hurriednesses'],
} 

describe('ghostBot', function() {
    beforeEach(function() {
         sinon.stub(wordService, 'getDictionary', () => {
             return Promise.resolve(dictionary);
         });
    });    
    
    describe('getNextPrefix', () => {
        it('should return the next letter of a found preffix', () => {
            return ghostBot.getNextPrefix('he').should.eventually.to.equal('hel');                                   
        })

        it('should return the next letter of a shorter word', () => {
            return ghostBot.getNextPrefix('hurr').should.eventually.to.equal('hurra');                                   
        }) 
         
        it('should be rejected if no word exists', () => {
            return ghostBot.getNextPrefix('xyz').should.be.rejectedWith("No words found");                                   
        }) 
                  
        it('should return a word if no other options', () => {
            return ghostBot.getNextPrefix('hurrahe').should.eventually.to.equal('hurrahed');                                   
        }) 

        it('should be rejected if a complete word is passed', () => {
            return ghostBot.getNextPrefix('bagels').should.be.rejectedWith("No words found");                                   
        }) 
                          
        it('should return another preffix if exist a longer word', () => {
            return ghostBot.getNextPrefix('hurrah').should.eventually.to.equal('hurrahe');                                   
        }) 
                  
    });
    
    afterEach(function() {
        wordService.getDictionary.restore();
    });
});    