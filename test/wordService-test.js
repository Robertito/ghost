var os = require('os');
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
var sinon = require('sinon')
var should = chai.should();
 
var wordService = require('../server/wordService')
var fs = require('fs') 

chai.use(chaiAsPromised);

const words = { 'h' : ['hello', 'hobbyists'] }

describe('wordService', function() {
    beforeEach(function() {
         sinon.stub(fs, 'readFile', (n,e,done) => {
             done(null,'hello' + os.EOL + 'hobbyists')
         });
    });    

    describe('getDictionary', () => {
        it('should return a dictionary of words', () => {
            return wordService.getDictionary().should.eventually.to.deep.equal(words);                                   
        }) 
    });
    
    describe('existLongerWords', () => {
        it('should return false if the word is complete and there are no others', () => {
            return wordService.existLongerWords('hello').should.eventually.be.false;
        })
        
        it('should return true if the preffix exists and it is not a word', () => {
            return wordService.existLongerWords('hell').should.eventually.be.true;
        })

        it('should return false if the word does not exists', () => {
            return wordService.existLongerWords('abc').should.eventually.be.false;
        })                          
    });
    
    afterEach(function() {
        fs.readFile.restore();
    });
              
  });    