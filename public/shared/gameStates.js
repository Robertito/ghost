const INVALID_WORD = "Invalid word";
const WORD_COMPLETED = "Word completed";
const RUNNING = "Game running";
const PLAYER1_WINS = "Player 1 wins";
const BOT_WINS = "Bot wins";


module.exports = {
    INVALID_WORD: INVALID_WORD,
    WORD_COMPLETED: WORD_COMPLETED,
    RUNNING: RUNNING,
    PLAYER1_WINS: PLAYER1_WINS,
    BOT_WINS: BOT_WINS
}