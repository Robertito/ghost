import GameController from './game-controller'

let GameDashboard = {
    templateUrl : '/app/game-view.html',
    controller : GameController,
    controllerAs: 'vm'
}


export default GameDashboard;