import gameStates from '../shared/gameStates'

class GameController {

    /*@ngInject*/
    constructor($http) {        
        this.http = $http;
    }

    sendWord (form) {
        if(form.$invalid) return;
        
        // If result is truthy the game
        // has ended so reset
        if(this.result) {
            this.word = '';
        }
        this.word = (this.word || '') + this.selectedLetter;
        this.http.post('/play/' + this.word)
            .then(res => {
                let responseObj = res.data;
                this.msg = responseObj.msg;
                this.result = responseObj.result;
                this.word = responseObj.word;
            },
            err => {
                if(err.status === 400) {
                    this.msg = err.data;
                    this.word = '';
                }
                console.error(err);
            });
        this.selectedLetter = '';
        form.$setPristine();
    }
}

export default GameController;