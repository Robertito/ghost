var express = require('express');
var router = express.Router();
const wordService = require('../server/wordService');
const bot = require('../server/ghostBot');
const gameService = require('../server/gameService');

router.post('/:word', function(req, res, next) {
  
  const re = /^[A-Za-z]+$/;
  let word = req.params.word;
  if (!re.test(word)) {
    let e = 'Incorrect word, only letters allowed';
    e.status = 400;
    return res.status(400).send(e);    
  } 
    
  word = word.toLowerCase()
  gameService.playAgainstTheBot(word)
    .then(data => {
      res.send(data);
    })
    .catch(e => next(new Error(e)));
});

module.exports = router;
