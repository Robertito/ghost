var command = "webpack";
if (process.env.NODE_ENV === 'production') {
    command = "webpack --config webpack.production.js";
}

  // We basically just create a child process that will run
  // the production bundle command
  var child_process = require('child_process');
  child_process.exec(command, function (error, stdout, stderr) {
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);
    if (error !== null) {
      console.log('exec error: ' + error);
    }
  });