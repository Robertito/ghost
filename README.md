# Ghost #

Implementation of the game of Ghost between a human player and a computer.

You can play at 
[The Ghost Game](https://ghostgame.herokuapp.com/)

### Stack used ###

* Server side implemented with NodeJS
* Client side implemented with Angular 1.5
* Webpack as a build tool
* Mocha as the test runner
* Heroku as the web platform
* Git as the version control system

### Setup ###

Clone the repository and run:
```javascript
npm install
```
Usage:
```javascript
npm start
```
Then navigate to http://localhost:3000 and start playing!

## Server side ##
Implemented in NodeJS using *express* as the web server which was created using 
*express generator* because the scaffolded code covered the requirements.

Entry point is under bin/www file but the app.js configures the web server.

### app.js ###
Only specific code to point out is the `require('./server/initializer')()` which
calls some initialization code. Removed unused middleware.

### views folder ###
Contains the views rendered in the server side. The *jade* view engine has been
used for simplicity. More complex views will be rendered in the client side as 
the application intend is to be a Single Page Application.

#### layout.jade ####
Note that only required one javascript tag with all the dependencies bundled (and minified
in production mode) into one file `script(src='/dist/bundle.js')`. 

#### index.jade ####
Only contains two tags, one to identify the angular app directive `div(ng-app='app')` and 
another one with the angular component root will contain the rest of the components
which will be rendered in the client `app-root`.

### routes folder ###
Only two routes required, the `GET `/ to return the *index.html* and the main route
used to play which will be `POST /play/:word` accepting the word as a URL parameter
instead of body data for simplicity. Used a POST because it makes more semantic sense
than a GET but both options would work similarly.

#### play.js ####
Validates that the passed parameter only contains letters, otherwise returns a `400 BadRequest`.
Calls the *gameService* component and returns the data or any internal error. No
more funcionality than required has been implememnted in the routes components.

### server folder ###
This folder contains the server logic of the application. For bigger applications, the 
components would have been split by features in different folders.

#### wordService.js ####
This component is in charge of working with the dictionary, it abstract away if the 
words are in a file, a database or a web service publishing an API that other components
can check but hidding the inner data structure used.

After comparing three data structures it was decided to use a Javascript object acting as
a dictionary due to a mix of good performance and simplicity. If the performance was critical
due to many more words added to the list 
other option could be a Radix tree which
it was tried but no improvement in performance was noticed so it was discarded due to extra
complexity with no added value.  

#### gameService.js ####
This service is the *core* of the application, it validates that the word received is 
valid, then it calls the service that acts as a computer player and then send the payload
back.

#### ghostBot.js ####
This is the *brain* of the application, it acts as the computer player, the strategy followed
is to find words with one more character than returned from the dictionary. It uses 
`lodash` for filtering as it is really quick.

## Client side ##
Implemented using angular 1.5. Some options were considered like angular 2 but it was
discarded due to lack of maturity. React and Redux were also considered but I just wanted
to get the feeling of working with angular again as I have been practicing a lot with React
and Redux lately.

Used ES2015 syntax being transpiled using *Babel* through *webpack* which also does 
the bundling creating a unique javascript file. In production mode (`NODE_ENV === 'production'`) 
the bundle.js is minified.

The architecture of the client side is simple, `app.js` is the bootstrapper that just
creates the component `game-dashboard.js`. That component *binds* the view `game-view.html`
with the controller `game-controller.js` which does the logic. Creating other services
has not been required but if application grows more components (like services) would 
be created to keep the controllers as thin as possible.

### game-controller.js ###
Uses the angular service `$http` to communicate with the server. It uses the angular
form directives for validation purposes like `required` and `ng-maxlength`. No extra
validation has been required.

### game-view.html ###
Uses the angular form directives for validation and to display error messages.

### Created by ###

* Roberto Aranda López