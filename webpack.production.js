var path = require('path');
var webpack = require('webpack');
 
module.exports = {
  entry: path.join(__dirname, 'public', 'app', 'app.js'),
  output: {
    path: path.join(__dirname, 'public', 'dist'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
          test: /\.js$/,
          include: path.join(__dirname, 'public', 'app'),
          loaders: ['ng-annotate','babel-loader?presets[]=es2015'],
      }
    ]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin()    
  ]
};