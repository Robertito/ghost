const _ = require('lodash');
const fs = require('fs');
const os = require('os');

let dictionary = {};
let dictionaryTree = {};

function isValidPrefix(prefix) {
    return getDictionary()
        .then(d => {
            const idx = _.findIndex(d[prefix.substring(0,1)],
                w => w.startsWith(prefix));
            return idx !== -1;
        })
}

function existLongerWords(prefix) {
    return getDictionary()
        .then(d => {
            const idx = _.findIndex(d[prefix.substring(0,1)],
                w => w.startsWith(prefix) && prefix !== w);
            return idx !== -1;
        })
}

function readWordsFromFile () {
    return new Promise((solve, reject) => {
        fs.readFile('./word.lst', 'utf8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                solve(data.split(os.EOL));
            }                           
        });
    });
}

function populateDictionary () {
    if(_.isEmpty(dictionary)) {
        return readWordsFromFile()
            .then(ws => {
                let tmpLetter = '';
                ws.forEach((w,i) => {
                    let currentLetter = w.substring(0,1);
                    if(currentLetter !== tmpLetter) {
                        tmpLetter = currentLetter;
                    }
                    if(dictionary[currentLetter]) {
                        dictionary[currentLetter].push(w);
                    } else {
                        dictionary[currentLetter] = [w];
                    }
                });
            });
    }    
}

function getDictionary () {        
    if(_.isEmpty(dictionary)){        
        return populateDictionary().then(x => dictionary);
    } else {
        return Promise.resolve(dictionary);
    }
}


module.exports.getDictionary = getDictionary;
module.exports.existLongerWords = existLongerWords;
module.exports.isValidPrefix = isValidPrefix;
module.exports.populateDictionary = populateDictionary;
