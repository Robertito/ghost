var wordService = require('./wordService');

module.exports = function() {
    console.log('Populating dictionary');
    wordService.populateDictionary()
        .then(d => console.log('Dictionary populated'))
        .catch(err => console.log(err));
}