var wordService = require('./wordService')
var _ = require('lodash')
var gs = require('../public/shared/gameStates')
var bot = require('./ghostBot')

/*
    1.- Validate received word
    2.- If valid call the "Bot" player, else return invalid word
    3.- If word returned by Bot, check if more words exist
    4.- If no more words exist return word completed, else return word
    5.- If the error is handled return Invalid Word, else throw it
*/
function playAgainstTheBot(word) {
    return wordService.isValidPrefix(word)
        .then(valid => {
            if (valid) return wordService.existLongerWords(word);
            throw gs.INVALID_WORD;
        })
        .then(exist => {
            if (exist) return bot.getNextPrefix(word);
            return buildResponseObject(gs.WORD_COMPLETED,
                gs.BOT_WINS, word);
        })
        .then(new_word => {
            // No string returned, game ended
            if(typeof new_word === 'object') return new_word;
            
            // Nested promise as the new_word variable must be
            //  in the scope. An alternative could be async-await
            return wordService.existLongerWords(new_word)
                .then(exist => {
                    if (exist) { 
                        return buildResponseObject(gs.RUNNING,
                            null, new_word);
                    }                    
                    return buildResponseObject(gs.WORD_COMPLETED,
                        gs.PLAYER1_WINS, new_word);                    
                })
        })
        .catch(err => {
            switch (err) {
                case gs.INVALID_WORD:
                    return buildResponseObject(err,
                        gs.BOT_WINS, word);                        
                default:
                    throw err;
            }
        });
}

// Helper method, this will be the payload sent
function buildResponseObject(err, result, word) {
    return {
        msg: err,
        result: result,
        word: word
    }
}

module.exports.playAgainstTheBot = playAgainstTheBot;