const wordsService = require('./wordService');
const _ = require('lodash');

function getNextPrefix(word) {
    return wordsService.getDictionary()
        .then(ws => {            
            const founds = _.filter(ws[word.substring(0,1)], 
                w => w.startsWith(word));
            // Filter to get words with two more characters
            const filteredFounds = _.filter(founds,
                w => w.length > word.length + 2)
            let wordFound;
            // It selects the minimum length with more than one 
            // characters left to make the rival to loose
            if(filteredFounds.length > 0) {
                wordFound = _.minBy(filteredFounds, w => w.length);
            } else {
                wordFound = _.find(founds, w => w.length > word.length)
            }
            if(!wordFound) throw "No words found";

            return wordFound.substring(0,word.length + 1);
        })
}


module.exports.getNextPrefix = getNextPrefix;